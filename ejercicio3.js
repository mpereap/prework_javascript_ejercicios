var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];

var numero = prompt("Introduce el número de tu DNI sin la letra");

var letra = prompt("Introduce la letra de tu DNI en mayúsculas");

letra = letra.toUpperCase();

if(numero < 0 || numero > 99999999) {
  alert("El número introducido es incorrecto"); 
}
else {
  var letraCalculada = letras[numero % 23];
  if(letraCalculada != letra) {
    alert("DNI incorrecto");
  }
  else {
    alert("DNI válido");
  }
}